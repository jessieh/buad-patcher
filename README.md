# BUAD Patcher

A patch tool that uses [UndertaleModTool](https://github.com/krzys-h/UndertaleModTool) to programmatically apply patches/mods to [Buck Up and Drive!](https://store.steampowered.com/app/1714590)

## Usage

Place patches (`.csx` extension) in the `Scripts/` folder and run `Buck Up and Drive! Patcher.cmd` to apply them. 

UndertaleModTool will automatically be downloaded if not present, and all scripts will be executed.

The patch tool will automatically locate the game directory if it was installed through Steam. If the game cannot be located, the tool will prompt you for the game's install location.

## Writing Scripts

Scripts are executed using UndertaleModTool. For information on writing UMT scripts, see [the documentation](https://github.com/krzys-h/UndertaleModTool/wiki/UndertaleModTool-Scripting) and the [example scripts](https://github.com/krzys-h/UndertaleModTool/tree/master/UndertaleModTool/Scripts).

## Available Patches

 - [Custom BGM Patch](https://gitlab.com/jessieh/buad-custom-bgm-patch)

   Modifies the in-game BGM to stream from an external audio file.

Feel free to submit patches to be added to this list!
