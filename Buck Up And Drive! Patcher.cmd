@ECHO OFF

REM Buck Up And Drive! Patcher.cmd
REM Jessie Hildebrandt
REM This script will automatically download UndertaleModTool and apply the patches/mods in the Scripts directory

REM ---------------------------------------------------------------------------
REM --- Variable definitions

SET COLORS_NORMAL=3F
SET COLORS_INPUT=3E
SET COLORS_ERROR=4F
                                              
SET MOD_TOOL_URL=https://github.com/krzys-h/UndertaleModTool/releases/download/0.4.0.4/UndertaleModCli_v0.4.0.4.zip

SET MOD_TOOL_DIR=ModTool
SET MOD_TOOL_ZIP=%MOD_TOOL_DIR%\ModTool.zip
SET MOD_TOOL=%MOD_TOOL_DIR%\UndertaleModCli.exe

SET SCRIPTS_DIR=Scripts

SET GAME_FQDIR=%PROGRAMFILES(x86)%\Steam\steamapps\common\Buck Up And Drive!

SET "PS_FIND_FOLDER_CMD="(New-Object -COM 'Shell.Application').BrowseForFolder(0,'Please locate the Buck Up And Drive! game folder.',0,0).self.path""

REM ---------------------------------------------------------------------------
REM --- Window setup

COLOR %COLORS_NORMAL%
MODE CON:COLS=60 LINES=50
TITLE Buck Up And Drive! Patcher

ECHO:     ___  _  _ ____ _  _    _  _ ___     ____ _  _ ___  
ECHO:     ^|__] ^|  ^| ^|    ^|_/     ^|  ^| ^|__]    ^|__^| ^|\ ^| ^|  \ 
ECHO:     ^|__] ^|__^| ^|___ ^| \_    ^|__^| ^|       ^|  ^| ^| \^| ^|__/ 
ECHO:       __________________________    ________________
ECHO:       ___  __ \__  __ \___  _/_ ^|  / /__  ____/__  /
ECHO:       __  / / /_  /_/ /__  / __ ^| / /__  __/  __  / 
ECHO:       _  /_/ /_  _, _/__/ /  __ ^|/ / _  /___   /_/  
ECHO:       /_____/ /_/ ^|_^| /___/  _____/  /_____/  (_)   
ECHO:    
ECHO:                         Patcher
ECHO:

REM ---------------------------------------------------------------------------
REM --- Download mod tool

WHERE /Q curl
IF ERRORLEVEL 1 (

    COLOR %COLORS_ERROR%

    ECHO:
    ECHO: ERROR
    ECHO: cURL not found in PATH! cURL is included in Windows 10 build versions 17063 and later.
    ECHO:

    PAUSE
    EXIT /B

)

IF NOT EXIST "%MOD_TOOL%" (

    IF NOT EXIST "%MOD_TOOL_DIR%\" MKDIR %MOD_TOOL_DIR%    

    IF NOT EXIST "%MOD_TOOL_ZIP%" (
    
        ECHO:
        ECHO: Downloading mod tool...
        ECHO:
        
        curl --progress-bar --location %MOD_TOOL_URL% --output %MOD_TOOL_ZIP%
    
    )    

    ECHO:
    ECHO: Extracting mod tool...
    ECHO:
    
    POWERSHELL -Command "Expand-Archive -Force -Path %MOD_TOOL_ZIP% -DestinationPath %MOD_TOOL_DIR%"

) ELSE (

    ECHO:
    ECHO: Mod tool already downloaded...
    ECHO:
    
)

REM ---------------------------------------------------------------------------
REM --- Locate game directory

IF NOT EXIST "%GAME_FQDIR%\" (

    ECHO:
    ECHO: Couldn't find Buck Up And Drive! Please locate the game directory.
    ECHO:

    FOR /f "usebackq delims=" %%I IN (`POWERSHELL %PS_FIND_FOLDER_CMD%`) DO SET "GAME_FQDIR=%%I"

)

IF NOT EXIST "%GAME_FQDIR%\data.win" (

    COLOR %COLORS_ERROR%

    ECHO:
    ECHO: ERROR
    ECHO: Unable to find 'data.win' in the game directory at:
    ECHO: "%GAME_FQDIR%"
    ECHO:

    PAUSE
    EXIT /B

)

ECHO:
ECHO: Located Buck Up And Drive! at:
ECHO: "%GAME_FQDIR%"
ECHO:

IF NOT EXIST "%GAME_FQDIR%\data.win.backup" (

    ECHO:
    ECHO: Backing up game data before applying any patches...
    ECHO:

    COPY /Y "%GAME_FQDIR%\data.win" "%GAME_FQDIR%\data.win.backup"

) ELSE (

    ECHO:
    ECHO: Game data already backed up...
    ECHO:
    
)

REM ---------------------------------------------------------------------------
REM --- Apply patches

IF NOT EXIST "%SCRIPTS_DIR%\*.csx" ( 

    IF NOT EXIST "%SCRIPTS_DIR%\" MKDIR %SCRIPTS_DIR%

    COLOR %COLORS_ERROR%

    ECHO:
    ECHO: ERROR
    ECHO: No patches found in '%SCRIPTS_DIR%\'!
    ECHO: Patch files should have a '.csx' extension and should be placed in '%SCRIPTS_DIR%\'.
    ECHO:

    PAUSE
    EXIT /B

)

REM The following loop builds a string of the quoted fully qualified paths of each file in the 
REM scripts directory. Delayed expansion is required for the dynamic variable expansion ("!SCRIPTS!")
REM to work here. Additionally, we access "%%F" before enabling delayed expansion so that any "!"s in
REM the filepath are not eaten by the shell as special characters.

SET SCRIPTS_FQPATHS=
FOR /R %%F IN ("%SCRIPTS_DIR%\*.csx") DO (
    SET __SCRIPT_FILE="%%F"
    SETLOCAL ENABLEDELAYEDEXPANSION
    SET SCRIPTS_FQPATHS=!SCRIPTS_FQPATHS! !__SCRIPT_FILE!
    SETLOCAL DISABLEDELAYEDEXPANSION
)

ECHO:
ECHO: NOTE: The following patches will be applied:
ECHO:

FOR /R %%F IN ("%SCRIPTS_DIR%\*.csx") DO (
    ECHO:  - "%%~NF"
    ECHO:
)
ECHO:

COLOR %COLORS_INPUT%
SET /P DECISION=... Proceed (Y\N)? 
COLOR %COLORS_NORMAL%

IF /I "%DECISION%" NEQ "Y" (

    COLOR %COLORS_ERROR%

    ECHO:
    ECHO: Patch operation cancelled.
    ECHO:

    PAUSE
    EXIT /B

)

ECHO:
ECHO: Applying patches to game data...
ECHO:

%MOD_TOOL% load "%GAME_FQDIR%\data.win" --scripts %SCRIPTS_FQPATHS% --output "%GAME_FQDIR%\data.win"

REM ---------------------------------------------------------------------------
REM --- Finished

ECHO:
ECHO: Done! Hopefully we didn't break anything...
ECHO:
ECHO: In the event of total game breakage, replace
ECHO: the 'data.win' file with the 'data.win.backup'
ECHO: file in your game directory!
ECHO:

PAUSE